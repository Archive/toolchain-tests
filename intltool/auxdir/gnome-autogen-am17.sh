#!/bin/sh
# Run this to generate all the initial makefiles, etc.

if test ! -d autotools; then mkdir autotools; fi
REQUIRED_AUTOMAKE_VERSION=1.7 USE_GNOME2_MACROS=1 . gnome-autogen.sh
